<div class="col-md-4 col-12 sidebar pr-0 pl-0 d-none d-sm-none d-md-none d-lg-block">
    <div class="col-xs-12 pl-lg-5 pr-lg-5 pr-md-4 pl-md-4 pr-sm-4 pl-sm-4" style="background: #11305e; height: 90px;">
        <img src="images/instarget-white.png" srcset="images/instarget-white.png" class="img-fluid" style="margin-top: 32px;" />
    </div>

    <ul class="left-menu">
        <?php foreach ($menu as $icon => $item) : ?>
            <li class="">
                <a href="#">
                    <p style="display: table-cell;vertical-align: middle; padding-right: 17px;">
                        <i class="<?= $icon ?>" style="font-size: 1.3em; color: #c3dbff;"></i>
                    </p>

                    <span style="display: table-cell;vertical-align: middle;">
                                <?= $item ?>
                            </span>

                    <span class="float-right" style="display: table-cell;vertical-align: middle;">
                                <i class="fas fa-chevron-right" style="color: #c3dbff;"></i>
                            </span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>

    <div class="text-center mt-4" style="color: #2057a9; font-size: 0.8em;">&copy; InsTarget.pro, 2018</div>
</div>