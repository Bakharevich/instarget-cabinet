<div class="col-xs-12 pr-4 pt-3  d-none d-sm-none d-md-none d-lg-block" style="background: #FFF; height: 90px; box-shadow: 0 4px 2px -2px #d9dde5;">
    <div class="container-fluid">
        <div class="float-left pl-3 pt-2" style="font-size: 0.9em;">
            <a href="#" style="color: #7a7d8a;" class="float-left mr-5">
                <div class="text-center">
                    <i class="fab fa-vk" style="font-size: 1.1em"></i>
                    <br/>
                    Новости
                </div>
            </a>

            <a href="#" style="color: #7a7d8a;" class="float-left mr-5">
                <div class="text-center">
                    <i class="fas fa-ambulance" style="font-size: 1.1em"></i>
                    <br/>
                    Инструкция
                </div>
            </a>

            <a href="#" style="color: #7a7d8a;" class="float-left mr-5">
                <div class="text-center">
                    <i class="fas fa-users"></i>
                    <br/>
                    Реферал
                </div>
            </a>
        </div>

        <div class="dropdown float-right">
            <button style="background: #FFF; color: #6c757d;" class="btn btn-secondary dropdown-toggle mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Илья Бахаревич
            </button>

            <img src="images/tmp01.jpg" class="rounded-circle img-fluid dropdown-toggle" width="48" data-toggle="dropdown" />

            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Профиль</a>
                <a class="dropdown-item" href="#">Обновить пароль</a>
                <a class="dropdown-item" href="#">Биллинг</a>
            </div>
        </div>
    </div>
</div>