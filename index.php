<?php
$menu = [
    'fas fa-tachometer-alt' => 'Рабочий стол',
    'fas fa-user-circle' => 'Аккаунты',
    'fas fa-chart-line' => 'Аналитика',
    'fas fa-question-circle' => 'Помощь',
    'fas fa-sign-out-alt' => 'Выход'
];
$accounts = [
    [
        'id' => 1,
        'photo' => 'images/tmp01.jpg',
        'name' => 'Ilya Bakharevich',
        'login' => '@Bakharevich',
        'status' => 'Подключен'
    ],
    [
        'id' => 2,
        'photo' => 'images/tmp01.jpg',
        'name' => 'Test Testovich',
        'login' => '@testovich',
        'status' => 'Отключен'
    ]
];
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!--
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    -->
    <!--<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>-->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/custom.css" />

    <title>InsTarget v.2</title>

    <style>

    </style>
</head>
<body class="bg-light">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary navbar-toggleable-md d-md-block d-lg-none">
        <a class="navbar-brand" href="#">InsTarget</a>

        <div class="float-right">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php foreach ($menu as $icon => $item) : ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="<?= $icon ?>"></i> <?= $item ?></a>
                    </li>
                <?php endforeach; ?>

                <!--
                Пример, как сделать выпадающий дропдаун со списком аккаунтов

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                -->
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row flex-nowrap">
            <!-- Sidebar -->
            <?php require_once 'sidebar.php' ?>
            <!-- Right block -->
            <div class="col-md col-12 main pa-1 pl-0 pr-0">
                <?php require_once 'header.php' ?>

                <div class="container-fluid pt-3 pl-4 pr-4">
                    <h3 class="mb-4">Рабочий стол</h3>

                    <?php foreach ($accounts as $account) : ?>
                        <div class="my-3 p-3 bg-white rounded box-shadow mb-4">
                            <div class="row">
                                <div class="col-md-6" style="border-right: 1px solid #dfdfdf;">
                                    <div class="row mt-2">
                                        <div class="col-sm-4">
                                            <img src="<?= $account['photo'] ?>" class="rounded-circle" style="width: 100%; max-width: 200px;"  />
                                        </div>
                                        <div class="col-sm-8 pt-3">
                                            <h5 class="mb-0"><?= $account['name'] ?></h5>
                                            <small style="color: #888;"><?= $account['login'] ?></small>

                                            <div style="margin-top: 12px;">
                                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter<?= $account['id'] ?>" class="btn btn-sm <?php if ($account['status'] == 'Отключен') echo 'btn-danger'; else echo 'btn-secondary'; ?>" style="background: #FFF; color: #000;">
                                                    <?= $account['status'] ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="border-right: 1px solid #dfdfdf;">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Подписчики
                                            <span class="badge badge-secondary badge-pill">0</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Лайки
                                            <span class="badge badge-secondary badge-pill">0</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Комментарии
                                            <span class="badge badge-secondary badge-pill">0</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-success btn-block btn-sm">
                                        <i class="fas fa-plus"></i><br/>Добавить задачу
                                    </a>
                                    <button class="btn btn-secondary btn-block btn-sm">Статистика</button>
                                    <button class="btn btn-secondary btn-block btn-sm">Удалить</button>
                                </div>
                            </div>
                        </div>

                        <!-- Just for testing purposes -->
                        <div class="modal fade" id="exampleModalCenter<?= $account['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Авторизация в Instagram</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pt-5 pb-5">
                                        <div class="row">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-8">
                                                <form>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Логин Instagram</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Пароль</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">Войти</button>
                                                </form>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                        <!--
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /End of modal -->
                    <?php endforeach; ?>

                    <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading"><i class="fas fa-lightbulb" style="margin-right: 7px; color: #ffc107;"></i> С чего начать?</h4>
                        Начните работу c InsTarget.pro через добавление новой задачи. Для этого нажмите на "<i class="fas fa-plus"></i> Добавить задачу"
                        справа от Инстаграм аккаунта.
                    </div>

                    <!--
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <i class="fas fa-lightbulb" style="margin-right: 7px; color: #ffc107;"></i> Начните работу c InsTarget.pro через добавление новое задачи.
                    </div>
                    -->

                    <!--
                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            <h6 class="border-bottom border-gray pb-2 mb-0">Рабочий стол</h6>
                            <div class="media text-muted pt-3">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161ec803b17%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161ec803b17%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2212.125%22%20y%3D%2216.95000002384186%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">@username</strong>
                                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=e83e8c&amp;fg=e83e8c&amp;size=1" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161ec803b1b%20text%20%7B%20fill%3A%23e83e8c%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161ec803b1b%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23e83e8c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2212.125%22%20y%3D%2216.95000002384186%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">@username</strong>
                                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=6f42c1&amp;fg=6f42c1&amp;size=1" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161ec803b1d%20text%20%7B%20fill%3A%236f42c1%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161ec803b1d%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%236f42c1%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2212.125%22%20y%3D%2216.95000002384186%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">@username</strong>
                                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                                </p>
                            </div>
                            <small class="d-block text-right mt-3">
                                <a href="#">All updates</a>
                            </small>
                        </div>
                    -->

                </div>

            </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="node_modules/jquery/dist/jquery.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="fontawesome-free-5.0.8/svg-with-js/js/fontawesome-all.min.js"></script>
</body>
</html>